package main

import (
	"github.com/gin-gonic/gin"
	"os/exec"
  "net/http"
  "bytes"
  "fmt"
  "strings"
)

type PktgenArgs struct {
  Timeout    int     `json:"timeout,default=60" form:"timeout,default=60"`
  delay      int     `json:"delay,default=5" form:"delay,default=5"`
  Portnum    string  `json:"port_num,omitempty" form:"port_num,omitempty"`
  NumPackets int     `json:"num_packets,omitempty" form:"num_packets,omitempty"`
  PacketSize int     `json:"packet_size,omitempty" form:"packet_size,omitempty"`
  GbpsRate   float64 `json:"gbps_rate,omitempty" form:"gbps_rate,omitempty"`
  MppsRate   float64 `json:"mpps_rate,omitempty" form:"mpps_rate,omitempty"`
  SrcDstMac  string  `json:"src_dst_mac,omitempty" form:"src_dst_mac,omitempty"`
  SrcDstIP   string  `json:"src_dst_ip,omitempty" form:"src_dst_ip,omitempty"`
  SrcPort    int     `json:"src_port,omitempty" form:"src_port,omitempty"`
  DstPort    int     `json:"dst_port,omitempty" form:"dst_port,omitempty"`
}

func (args *PktgenArgs) GetNumPackets() string {
  if (args.NumPackets == 0) {
    return ""
  }
  return fmt.Sprintf(`-n '%d' `, args.NumPackets)
}

func (args *PktgenArgs) GetPacketSize() string {
  if (args.PacketSize == 0) {
    return ""
  }
  return fmt.Sprintf(`-s '%d' `, args.PacketSize)
}

func (args *PktgenArgs) GetGbpsRate() string {
  if (args.GbpsRate == 0) {
    return ""
  }
  return fmt.Sprintf(`-r '%f' `, args.GbpsRate)
}

func (args *PktgenArgs) GetMppsRate() string {
  if (args.MppsRate == 0) {
    return ""
  }
  return fmt.Sprintf(`-R '%f' `, args.MppsRate)
}

func (args *PktgenArgs) GetSrcDstMac() string {
  if (args.SrcDstMac == "") {
    return ""
  }
  return fmt.Sprintf(`-E '%s' `, args.SrcDstMac)
}

func (args *PktgenArgs) GetSrcDstIP() string {
  if (args.SrcDstIP == "") {
    return ""
  }
  return fmt.Sprintf(`-I '%s' `, args.SrcDstIP)
}

func (args *PktgenArgs) GetSrcPort() string {
  if (args.SrcPort == 0) {
    return ""
  }
  return fmt.Sprintf(`-S '%d' `, args.SrcPort)
}

func (args *PktgenArgs) GetDstPort() string {
  if (args.DstPort == 0) {
    return ""
  }
  return fmt.Sprintf(`-D '%d' `, args.DstPort)
}

func (args *PktgenArgs) ReturnPktgenArgs() string {
  var cmd strings.Builder
  cmd.WriteString(args.GetPacketSize())
  cmd.WriteString(args.GetNumPackets())
  cmd.WriteString(args.GetGbpsRate())
  cmd.WriteString(args.GetMppsRate())
  cmd.WriteString(args.GetSrcDstMac())
  cmd.WriteString(args.GetSrcDstIP())
  cmd.WriteString(args.GetSrcPort())
  cmd.WriteString(args.GetDstPort())
  return cmd.String()
}

func postSendPktgenTraffic(c *gin.Context) {
  args := PktgenArgs{}
  args.Timeout = 60 // default timeout
  args.delay = 5    // default command delay
  c.ShouldBind(&args)

  // check if flowgen process is already running. if it is, refuse the request
  flowgen_pid := "ps aux | awk '(/snf_pktgen/ || /run_pktgen/) && !/timeout/ && !/sleep/ {print $2}'"
  out, _ := exec.Command("sh", "-c", flowgen_pid).Output()
  if string(out) != "" {
    c.String(http.StatusServiceUnavailable, fmt.Sprintf("Pktgen Is running already.  PID: %s", string(out)))
    return
  }

  // cmd := fmt.Sprintf(`traffic_api/run_pktgen %d %d "%s"`, args.Timeout, args.delay, args.ReturnPktgenArgs())
  snf_pktgen := fmt.Sprintf("./snf_pktgen %s", args.ReturnPktgenArgs())
  cmd := fmt.Sprintf(`sleep %d && timeout %d %s >/dev/null 2>&1 &`, 
    args.delay, args.Timeout, snf_pktgen)
  PktgenCommand := exec.Command("bash", "-c" , cmd)
  fmt.Println(PktgenCommand)
  err := PktgenCommand.Start()
  if err != nil {
    fmt.Println(err)
    c.String(http.StatusInternalServerError, err.Error())
  }

  c.String(http.StatusOK, fmt.Sprintf("STARTED PKTGEN: %s\n", snf_pktgen))
}

func getPktgenArgs(c *gin.Context) {
  pktgenArgs := exec.Command("./snf_pktgen", "--help", "||", "true")
  var out bytes.Buffer
  pktgenArgs.Stdout = &out
  pktgenArgs.Run()

  c.String(http.StatusOK, out.String())
}

func main() {
	r := gin.Default()

	r.POST("/pktgen/start", postSendPktgenTraffic)
	r.GET("/pktgen/args", getPktgenArgs)

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}