FROM golang

WORKDIR /go/src/app
COPY src/build/fabric_edge_test/traffic_api .

RUN go mod init traffic_api
RUN go get -d -v ./...
RUN go install -v ./...
# RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o /docker-traffic_api
RUN go build -o /docker-traffic_api

EXPOSE 8080

CMD ["/docker-traffic_api"]