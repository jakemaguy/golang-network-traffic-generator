#!/usr/bin/env bash
docker stop traffic-api

docker build -t traffic-api -f src/build/fabric_edge_test/traffic_api/Dockerfile .

docker run -d --rm \
--privileged \
--pid=host --ipc=host \
-v /opt/snf/bin/tests/:/go/src/app -p 8080:8080 \
--name traffic-api traffic-api